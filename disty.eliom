[%%shared
    open Eliom_lib
    open Eliom_content
    open Html5.D
]

module Disty_app =
  Eliom_registration.App (
    struct
      let application_name = "disty"
    end)

let main_service =
  Eliom_service.App.service ~path:[] ~get_params:Eliom_parameter.unit ()

let () =
  Disty_app.register
    ~service:main_service
    (fun () () ->
       Lwt.return
         (Eliom_tools.F.html
            ~title:"disty"
            ~css:[["css";"disty.css"]]
            Html5.F.(body 
                       [h1 [pcdata "hello"]; img ~alt:"stream" ~src:(uri_of_string (fun () -> "/stream")) ()]
              )))

