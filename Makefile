all: build fft_client.native

build:
	ocamlbuild -use-ocamlfind -plugin-tags "package(eliom.ocamlbuild)" \
                                  server/disty.cma server/disty.cmxa server/disty.cmxs \
                                  client/disty.js -use-menhir

run: build
	mkdir -p _run/log/disty/
	mkdir -p _run/data/disty/ocsipersist
	rm _run/data/disty/ocsipersist/socket -f
	-killall mjpg_streamer -q
	mjpg_streamer --input "input_uvc.so --device /dev/video0 --fps 15" --output "output_http.so -w /usr/share/mjpeg-streamer/www/" &
	ocsigenserver -c disty.conf -v

clean:
	rm -rf _build

